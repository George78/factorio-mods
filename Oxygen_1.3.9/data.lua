require "config"
if data.raw.fluid["oxygen"] == nil then require "prototypes.oxygen" end
require "prototypes.oxygen-bottles"
require "prototypes.dispenser"
require "prototypes.gas-mask"
data:extend({
  {
	type = "damage-type",
	name = "oxygen"
  },
  {
	type = "item-subgroup",
	name = "oxygen",
	group = "intermediate-products",
	order = "d"
  }
})
if data.raw.technology["oxygen-production"] == nil then
	-- kein Chemistry
	data:extend({
	  {
		type = "technology",
		name = "oxygen-production",
		icon = "__Oxygen__/graphics/oxygen-production.png",
		icon_size = 128,
		effects = {
		  {
			type = "unlock-recipe",
			recipe = "empty-oxygen-bottle"
		  },
		  {
			type = "unlock-recipe",
			recipe = "big-empty-oxygen-bottle"
		  },
		  {
			type = "unlock-recipe",
			recipe = "oxygen-bottle"
		  },
		  {
			type = "unlock-recipe",
			recipe = "big-oxygen-bottle"
		  },
		  {
			type = "unlock-recipe",
			recipe = "oxygen-from-water"
		  },
		  {
			type = "unlock-recipe",
			recipe = "oxygen-from-atmosphere"
		  }
		},
		prerequisites = {"steel-processing"},
		unit = {
		  count = 25,
		  ingredients = {
			{"science-pack-1", 1},
			{"science-pack-2", 1}
		  },
		  time = 30
		},
		order = "d-b-a-a",
		upgrade = true
	  }
	})
else
	table.insert(data.raw.technology["oxygen-production"].prerequisites, "steel-processing")
	table.insert(data.raw.technology["oxygen-production"].effects, {type = "unlock-recipe", recipe = "empty-oxygen-bottle"})
	table.insert(data.raw.technology["oxygen-production"].effects, {type = "unlock-recipe", recipe = "big-empty-oxygen-bottle"})
	table.insert(data.raw.technology["oxygen-production"].effects, {type = "unlock-recipe", recipe = "oxygen-bottle"})
	table.insert(data.raw.technology["oxygen-production"].effects, {type = "unlock-recipe", recipe = "big-oxygen-bottle"})
end
data:extend({
  {
	type = "technology",
	name = "oxygen-production-2",
	icon = "__Oxygen__/graphics/oxygen-production.png",
	icon_size = 128,
	effects = {
	  {
		type = "unlock-recipe",
		recipe = "oxygen-dispenser"
	  }
	},
	prerequisites = {"oxygen-production"},
	unit = {
	  count = 75,
	  ingredients = {
		{"science-pack-1", 1},
		{"science-pack-2", 1},
		{"science-pack-3", 1}
	  },
	  time = 60
	},
	order = "d-b-a-b",
	upgrade = true
  }
})