require "config"
require "defines"
require "util"

remote.add_interface("oxygen", {
	changeoxygenofplayer = function(playername, amount)
		for _,v in ipairs(global.oxygen) do
			if v[1] == game.getplayer(playername) then
				v[2] = v[2] + amount
				updategui(v)
			end
		end
	end,
	setoxygenofplayer = function(playername, amount)
		for _,v in ipairs(global.oxygen) do
			if v[1] == game.getplayer(playername) then
				if amount > 100 then amount = 100 elseif amount < 0 then amount = 0 end
				v[2] = amount
				updategui(v)
			end
		end
	end,
	getoxygenofplayer = function(playername)
		for _,v in ipairs(global.oxygen) do
			if v[1] == game.getplayer(playername) then
				return v[2]
			end
		end
	end,
	hasgasmask = function(playername)
		return hasgasmask(game.getplayer(playername))
	end
})

script.on_init(function()
	if global.oxygen == nil then
		global.oxygen = {}
	end
end)

script.on_event(defines.events.on_tick, function(event)
	handleplayers()
	handledispensers()
	handleenemypositions()
end)

script.on_event(defines.events.on_player_created, function(event)
	if game.player.controller_type ~= defines.controllers.character then
		return
	end
	local player = game.get_player(event.player_index)
	
	table.insert(global.oxygen, {player, 100})
	
	local frame = player.gui.left.add({type="frame", name="oxygen-amount", direction="horizontal"})
	frame.add({type="label", name="caption-label", caption={"oxygen"}})
	frame.add({type="label", name="oxygen-value", caption=tostring(100)})
	
	player.get_inventory(defines.inventory.player_main).insert({name="big-oxygen-bottle", count=10, health=1.0})
end)

function updategui(oxygenplayer)
	oxygenplayer[1].gui.left["oxygen-amount"]["oxygen-value"].caption = tostring(oxygenplayer[2])
end

function checkinventory(oxygenplayer)
	local inv = oxygenplayer[1].get_inventory(defines.inventory.player_main)
	
	for i=1,2 do
		if inv.get_item_count("oxygen-bottle") >= 1 and oxygenplayer[2] <= 50 then
			inv.remove{name="oxygen-bottle", count=1}
			inv.insert{name="empty-oxygen-bottle", count=1}
			oxygenplayer[2] = oxygenplayer[2] + 50
		elseif inv.get_item_count("big-oxygen-bottle") >= 1 and oxygenplayer[2] == 0 then
			inv.remove{name="big-oxygen-bottle", count=1}
			inv.insert{name="big-empty-oxygen-bottle", count=1}
			oxygenplayer[2] = oxygenplayer[2] + 100
		end
		inv = oxygenplayer[1].get_inventory(defines.inventory.player_quickbar)
	end
end

function hasgasmask(player)
	if player.get_inventory(defines.inventory.player_armor).is_empty then return false end
	if not player.get_inventory(defines.inventory.player_armor)[1].has_grid then return false end
	local grid = player.getinventory(defines.inventory.playerarmor)[1].grid
	for _,equip in pairs(grid.equipment) do
		if equip.name == "gas-mask" then return true end
	end
	return false
end

entityBuilt = function(event)
	if event.created_entity ~= nil and event.created_entity.valid and event.created_entity.name == "oxygen-dispenser" then
		if global.dispensers == nil then
			global.dispensers = {}
		end
		table.insert(global.dispensers, {event.created_entity, 0})
	end
end

entityRemoved = function(event)
	if event.entity ~= nil and event.entity.valid and event.entity.name == "oxygen-dispenser" then
		for i,v in ipairs(global.dispensers) do
			if v[1].equals(event.entity) then
				table.remove(global.dispensers, i)
			end
		end
		if #global.dispensers == 0 then global.dispensers = nil end
	end
end

function handledispensers()
	if global.dispensers == nil or game.tick % 60 ~= 0 then return end
	for i,v in ipairs(global.dispensers) do
		--game.player.print(v[2])
		if v[2] > 0 then
			local fluid = v[1].fluidbox[1]
			if fluid ~= nil and fluid.type == "oxygen" and fluid.amount >= 1 then
				fluid.amount = fluid.amount - 1
				v[1].fluidbox[1] = fluid
				v[2] = v[2] - 1
			else
				v[2] = 0
			end
		end
		if v[2] == 0 then
			local fluid = v[1].fluidbox[1]
			if fluid ~= nil and fluid.type == "oxygen" and fluid.amount >= 20 then
				v[2] = 20
				game.createentity({name="oxygen-cloud", position=v[1].position})
			end
		end
	end
end

function handleplayers()
	if game.tick % 25 ~= 0 then return end
	for _,v in ipairs(global.oxygen) do
		if v[1].controller_type == defines.controllers.character then
			
			-- Sauerstoffverbrauch und Schaden bei geringem Sauerstoff
			if v[2] > 0 and (game.tick % 800 == 0 or (not hasgasmask(v[1]) and game.tick % 400 == 0)) then
				v[2] = v[2] - 1
				updategui(v)
			elseif v[2] <= 0 and game.tick % 300 == 0 and v[1].character ~= nil then
				if hasgasmask(v[1]) then
					v[1].character.damage(--[[(((game.getpollution(v[1].character.position) / 1000) * 12.5) * 0.75)]]5, v[1].force, "oxygen")
				else
					v[1].character.damage(--[[((game.getpollution(v[1].character.position) / 1000) * 12.5)]]10, v[1].force, "oxygen")
				end
			end
			checkinventory(v)
			
			-- Auffüllung durch Verteiler
			local pos = v[1].position
			if game.tick % 75 == 0 then
				entities = game.get_surface(1).find_entities_filtered{area={{pos.x - 10, pos.y - 10}, {pos.x + 10, pos.y + 10}}, name="oxygen-dispenser"}
				if #entities > 0 then
					if v[2] < 100 then
						v[2] = v[2] + 1
						updategui(v)
					end
					if game.tick % 225 == 0 then
						local inv = v[1].get_inventory(defines.inventory.player_main)
						for i=1,2 do
							if inv.get_item_count("empty-oxygen-bottle") > 0 then
								inv.remove({name="empty-oxygen-bottle", count=1})
								inv.insert({name="oxygen-bottle", count=1})
							elseif inv.get_item_count("big-empty-oxygen-bottle") > 0 then
								inv.remove({name="big-empty-oxygen-bottle", count=1})
								inv.insert({name="big-oxygen-bottle", count=1})
							end
							inv = v[1].get_inventory(defines.inventory.player_quickbar)
						end
					end
				end
			end
			
		end
	end
end

function handleenemypositions()
	if global.dispensers == nil or game.tick % 100 ~= 0 then return end
	for _,v in ipairs(global.dispensers) do
		if v[2] > 0 then
			local enemies = game.findenemyunits(v[1].position, 10)
			if #enemies > 0 then
				for _,e in ipairs(enemies) do
					e.damage(5, e.force, "oxygen")
				end
			end
			local pos = v[1].position
			local spawners = game.findentitiesfiltered{area={{pos.x - 10, pos.y - 10}, {pos.x + 10, pos.y + 10}}, type="unit-spawner"}
			if #spawners > 0 then
				for _,s in ipairs(spawners) do
					s.damage(10, s.force, "oxygen")
				end
			end
		end
	end
end

script.on_event(defines.events.on_built_entity, entityBuilt)
script.on_event(defines.events.on_robot_built_entity, entityBuilt)

script.on_event(defines.events.on_preplayer_mined_item, entityRemoved)
script.on_event(defines.events.on_robot_pre_mined, entityRemoved)
script.on_event(defines.events.on_entity_died, entityRemoved)