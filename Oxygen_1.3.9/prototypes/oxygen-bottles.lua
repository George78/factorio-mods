data:extend({
  {
	type = "item",
	name = "empty-oxygen-bottle",
	icon = "__Oxygen__/graphics/empty-oxygen-bottle.png",
	flags = {"goes-to-main-inventory"},
	subgroup = "oxygen",
	order = "a[empty-bottle]-a[empty-oxygen-bottle]",
	stack_size = 5
  },
  {
	type = "item",
	name = "big-empty-oxygen-bottle",
	icon = "__Oxygen__/graphics/big-empty-oxygen-bottle.png",
	flags = {"goes-to-main-inventory"},
	subgroup = "oxygen",
	order = "a[empty-bottle]-b[big-empty-oxygen-bottle]",
	stack_size = 5
  },
  {
	type = "item",
	name = "oxygen-bottle",
	icon = "__Oxygen__/graphics/oxygen-bottle.png",
	flags = {"goes-to-main-inventory"},
	subgroup = "oxygen",
	order = "b[filled-bottle]-a[oxygen-bottle]",
	stack_size = 5
  },
  {
	type = "item",
	name = "big-oxygen-bottle",
	icon = "__Oxygen__/graphics/big-oxygen-bottle.png",
	flags = {"goes-to-main-inventory"},
	subgroup = "oxygen",
	order = "b[filled-bottle]-b[big-oxygen-bottle]",
	stack_size = 5
  },
  
  {
	type = "recipe",
	name = "oxygen-bottle",
	category = "crafting-with-fluid",
	enabled = "false",
	ingredients =
	{
	  {type="fluid", name="oxygen", amount=50},
	  {type="item", name="empty-oxygen-bottle", amount=1}
	},
	result = "oxygen-bottle"
  },
  {
	type = "recipe",
	name = "big-oxygen-bottle",
	category = "crafting-with-fluid",
	enabled = "false",
	ingredients =
	{
	  {type="fluid", name="oxygen", amount=100},
	  {type="item", name="big-empty-oxygen-bottle", amount=1}
	},
	result = "big-oxygen-bottle"
  },
  
  {
	type = "recipe",
	name = "empty-oxygen-bottle",
	enabled = "false",
	ingredients =
	{
	  {"steel-plate", 1}
	},
	result = "empty-oxygen-bottle"
  },
  {
	type = "recipe",
	name = "big-empty-oxygen-bottle",
	enabled = "false",
	ingredients =
	{
	  {"empty-oxygen-bottle", 1},
	  {"steel-plate", 1}
	},
	result = "big-empty-oxygen-bottle"
  },
})