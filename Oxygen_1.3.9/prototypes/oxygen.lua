data:extend({
  {
	type = "fluid",
	name = "oxygen",
	default_temperature = 25,
	heat_capacity = "1KJ",
	base_color = {r=0.7, g=0.7, b=0.7},
	flow_color = {r=0.5, g=0.5, b=0.5},
	max_temperature = 100,
	icon = "__Oxygen__/graphics/oxygen.png",
	pressure_to_speed_ratio = 0.4,
    flow_to_energy_ratio = 0.59,
    order = "a[fluid]-h[oxygen]"
  }
})
if data.raw.recipe["water-electrolysis"] == nil then
	data:extend({
	  {
		type = "recipe",
		name = "oxygen-from-water",
		subgroup = "oxygen",
		enabled = "false",
		order = "e-a",
		category = "chemistry",
		ingredients = {
		  {type="fluid", name="water", amount=2}
		},
		results = {
		  {type="fluid", name="oxygen", amount=3}
		}
	  },
	  {
		type = "recipe",
		name = "oxygen-from-atmosphere",
		subgroup = "oxygen",
		energy_required = 3,
		enabled = "false",
		order = "e-b",
		category = "chemistry",
		ingredients = {},
		results = {
		  {type="fluid", name="oxygen", amount=1}
		}
	  }
	})
end