data:extend({
  {
    type = "night-vision-equipment",
    name = "gas-mask",
    sprite = 
    {
      filename = "__Oxygen__/graphics/gas-mask.png",
      width = 96,
      height = 64,
      priority = "medium"
    },
    shape =
    {
      width = 3,
      height = 2,
      type = "full"
    },
    energy_source =
    {
      type = "electric",
      buffer_capacity = "0J",
      input_flow_limit = "0W",
      usage_priority = "primary-input"
    },
    energy_input = "0W",
    tint = {r = 0, g = 0, b = 0, a = 0}
  },
  {
	type = "item",
	name = "gas-mask",
	icon = "__Oxygen__/graphics/gas-mask-icon.png",
	flags = {"goes-to-main-inventory"},
	placed_as_equipment_result = "gas-mask",
	subgroup = "oxygen",
	order = "d[oxygen-equipment]-a[gas-mask]",
	stack_size = 1
  },
  {
	type = "recipe",
	name = "gas-mask",
	enabled = "false",
	ingredients =
	{
	  {"advanced-circuit", 5},
	  {"steel-plate", 10}
	},
	result = "gas-mask"
  },
  
  {
	type = "technology",
	name = "gas-mask",
	icon = "__Oxygen__/graphics/gas-mask-tech.png",
	icon_size = 128,
	effects = {
	  {
		type = "unlock-recipe",
		recipe = "gas-mask"
	  }
	},
	prerequisites = {"oxygen-production-2", "armor-making-3"},
	unit = {
      count = 100,
      ingredients = {
        {"science-pack-1", 1},
        {"science-pack-2", 1},
		{"science-pack-3", 1}
      },
      time = 30
    },
    order = "g-p"
  }
})