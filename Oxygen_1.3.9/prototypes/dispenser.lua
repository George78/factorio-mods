pipecoverspictures = function()
  return {
    north =
    {
      filename = "__base__/graphics/entity/pipe-covers/pipe-cover-north.png",
      priority = "extra-high",
      width = 44,
      height = 32
    },
    east =
    {
      filename = "__base__/graphics/entity/pipe-covers/pipe-cover-east.png",
      priority = "extra-high",
      width = 32,
      height = 32
    },
    south =
    {
      filename = "__base__/graphics/entity/pipe-covers/pipe-cover-south.png",
      priority = "extra-high",
      width = 46,
      height = 52
    },
    west =
    {
      filename = "__base__/graphics/entity/pipe-covers/pipe-cover-west.png",
      priority = "extra-high",
      width = 32,
      height = 32
    }
  }
end

data:extend({
  {
	type = "storage-tank",
    name = "oxygen-dispenser",
    icon = "__Oxygen__/graphics/dispenser-icon.png",
    flags = {"placeable-neutral", "player-creation"},
    minable = {mining_time = 1, result = "oxygen-dispenser"},
    max_health = 100,
    corpse = "small-remnants",
    collision_box = {{-0.35, -0.35}, {0.35, 0.35}},
    selection_box = {{-0.5, -0.5}, {0.5, 0.5}},
	window_bounding_box = {{-0.125, 0.6875}, {0.1875, 1.1875}},
    pictures =
	{
		picture =
		{
			sheet =
			{
				filename = "__Oxygen__/graphics/dispenser.png",
				priority = "extra-high",
				width = 32,
				height = 32,
				frames = 1,
				shift = {0, 0}
			},
		},
      fluid_background =
      {
        filename = "__Oxygen__/graphics/empty.png",
        priority = "extra-high",
        width = 1,
        height = 1
      },
      window_background =
      {
        filename = "__Oxygen__/graphics/empty.png",
        priority = "extra-high",
        width = 1,
        height = 1
      },
      flow_sprite =
      {
        filename = "__Oxygen__/graphics/empty.png",
        priority = "extra-high",
        width = 1,
        height = 1
      }
	},
	fluid_box =
    {
      base_area = 3,
	  pipe_covers = pipecoverspictures(),
      pipe_connections =
      {
        { position = {0, -1} },
        { position = {1, 0} },
        { position = {0, 1} },
        { position = {-1, 0} }
      },
    },
	flow_length_in_ticks = 360,
	vehicle_impact_sound =  { filename = "__base__/sound/car-metal-impact.ogg", volume = 0.65 },
    working_sound =
    {
      sound = {
          filename = "__base__/sound/storage-tank.ogg",
          volume = 0.8
      },
      apparent_volume = 1.5,
      max_sounds_per_type = 3
    },
    circuit_wire_connection_points =
    {
      {
        shadow =
        {
          red = {2.6875, 1.3125},
          green = {2.6875, 1.3125},
        },
        wire =
        {
          red = {1.1875, -0.28125},
          green = {1.1875, -0.28125},
        }
      },
      {
        shadow =
        {
          red = {0.21875, 1.1875},
          green = {0.21875, 1.1875},
        },
        wire =
        {
          red = {-1, -0.25},
          green = {-1, -0.25},
        }
      },
      {
        shadow =
        {
          red = {2.6875, 1.3125},
          green = {2.6875, 1.3125},
        },
        wire =
        {
          red = {1.1875, -0.28125},
          green = {1.1875, -0.28125},
        }
      },
      {
        shadow =
        {
          red = {0.21875, 1.1875},
          green = {0.21875, 1.1875},
        },
        wire =
        {
          red = {-1, -0.25},
          green = {-1, -0.25},
        }
      }
    },

    circuit_wire_max_distance = 7.5
  },
  {
	type = "item",
	name = "oxygen-dispenser",
	icon = "__Oxygen__/graphics/dispenser-icon.png",
	flags = {"goes-to-quickbar"},
	subgroup = "oxygen",
	order = "c[oxyge-devices]-a[oxygen-dispenser]",
	stack_size = 10,
	place_result = "oxygen-dispenser"
  },
  {
	type = "recipe",
	name = "oxygen-dispenser",
	enabled = "false",
	ingredients =
	{
	  {"steel-plate", 10},
	  {"electronic-circuit", 20},
	  {"pipe", 5}
	},
	result = "oxygen-dispenser"
  },
  
  
  {
    type = "smoke",
    name = "oxygen-cloud",
    flags = {"not-on-map"},
    show_when_smoke_off = true,
    animation =
    {
      filename = "__base__/graphics/entity/cloud/cloud-45-frames.png",
      priority = "low",
      width = 256,
      height = 256,
      frame_count = 45,
      animation_speed = 3,
      line_length = 7,
      scale = 3,
    },
    slow_down_factor = 0,
    wind_speed_factor = 0,
    cyclic = true,
    duration = 60 * 21,
    fade_away_duration = 60,
    spread_duration = 10,
    color = {r=0.5, g=0.5, b=0.5, a=0.1}
  },
})